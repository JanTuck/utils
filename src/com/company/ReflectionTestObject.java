package com.company;

/**
 * Created by Jan on 17-02-2017.
 */
public class ReflectionTestObject {
    private String field1;

    private Object field2;

    public ReflectionTestObject() {
    }

    public ReflectionTestObject(String field1) {
        this.field1 = field1;
    }

    public ReflectionTestObject(Object field2) {
        this.field2 = field2;
    }

    public ReflectionTestObject(String field1, Object field2) {
        this.field1 = field1;
        this.field2 = field2;
    }

    public void print() {
        System.out.println("Field1 = " + field1);
    }

    public void print(String test) {
        System.out.println("Test = " + test);
    }

    private void printField() {
        System.out.println("Field1 = " + field1);
    }


}
