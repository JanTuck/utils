package com.company;

import reflection.ReflectionUtil;

public class Main {

    public static void log(Object object) {
        if (object == null)
            return;
        System.out.println(object.toString());
    }

    public static void main(String[] args) throws Exception {
        ReflectionTestObject test1 = new ReflectionTestObject("Test", "Well this can be whatever i want but now its a string");
        ReflectionUtil.newCall().getField(test1.getClass(), "field1").get()
                .setAccessible(true)
                .passIfValid(reflectionField -> log(reflectionField.get(test1)))
                .newCall()
                .getConstructor(test1.getClass(), String.class)
                .get()
                .passIfValid(reflectionConstructor -> {
                    Object two = reflectionConstructor.newInstance("This is so fun!");
                    ReflectionUtil.newCall().getMethod(two.getClass(), "print").get().invoke(two);
                });
    }

}